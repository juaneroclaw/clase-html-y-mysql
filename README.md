# Clase HTML y Mysql

## Instalar Xampp

Lo primero que vamos hacer es instalar <b>XAMPP</b>

-   Ingresamos a [www.apachefriends.org/es/index.html](https://www.apachefriends.org/es/index.html)
- Hacemos click para el tipo de sistema que estamos trabajando, en mi caso es *Windows*
- Descargamos el .exe
- Una vez descargado instalamos el programa
- Hacemos click en *Siguiente*->*Siguiente*->*Siguiente*
- Nos va a crear una carpeta en el disco <b>C</b> llamada *XAMPP*
- Cuando este por terminar le damos *Permiso* al apache 
- Listo tenemos instalado XAMPP en nuestra pc

![Paso a Paso instalación del XAMPP](img/instalar%20xampp.gif)

## Instalar HeidiSQL

Ahora vamos a instalar <b>HeidiSQL</b>

- Ingresamos a [www.heidisql.com](https://www.heidisql.com/) 
- Vamos a la seccion "Downloads" descargamos en instalable (Esta disponible el portable, pero utilizaremos la version instalable)
- Hacemos click en <b>Installer, 32/64 bit combined</b>
- Una vez descargado instalamos el programa
- Hacemos click en *siguiente* -> *siguiente* -> *Instalar*
- Listo tenemos instalado HeidiSQL en nuestra PC

![Paso a paso instalación de HeidiSQL](img/Instalar%20Heidi%20SQL.gif)


## Descargar Archivos

- [Crear](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) o [Descargar](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) archivos
- [ ] [Agregar archivos usando la línea de comando](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) o envíe un repositorio Git existente con el siguiente comando:

```
cd existing_repo
git remote add origin https://gitlab.com/juaneroclaw/clase-html-y-mysql.git
git branch -M main
git push -uf origin main
```
