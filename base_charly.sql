-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.24-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para charly
CREATE DATABASE IF NOT EXISTS `charly` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `charly`;

-- Volcando estructura para tabla charly.banda
CREATE TABLE IF NOT EXISTS `banda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  KEY `Índice 1` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Volcando datos para la tabla charly.banda: ~6 rows (aproximadamente)
INSERT INTO `banda` (`id`, `nombre`) VALUES
	(1, 'Sui Generis'),
	(2, 'PorSuiGieco'),
	(3, 'La Maquina de Hacer Pájaros'),
	(4, 'Billy Bond and The Jets'),
	(5, 'Serú Girán'),
	(6, 'Solista');

-- Volcando estructura para tabla charly.discografia
CREATE TABLE IF NOT EXISTS `discografia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `id_banda` int(11) DEFAULT NULL,
  `img` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `id_tipo_cd` int(11) DEFAULT NULL,
  KEY `Índice 1` (`id`),
  KEY `FK_dicografia_banda` (`id_banda`),
  KEY `FK_discografia_tipo_cd` (`id_tipo_cd`),
  CONSTRAINT `FK_dicografia_banda` FOREIGN KEY (`id_banda`) REFERENCES `banda` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_discografia_tipo_cd` FOREIGN KEY (`id_tipo_cd`) REFERENCES `tipo_cd` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Volcando datos para la tabla charly.discografia: ~50 rows (aproximadamente)
INSERT INTO `discografia` (`id`, `nombre`, `year`, `id_banda`, `img`, `id_tipo_cd`) VALUES
	(1, 'Vida', 1973, 1, 'img/vida.jpg', 1),
	(2, 'Confesiones de invierno', 1973, 1, 'img/confesion.jpg', 1),
	(3, 'Pequeñas anécdotas sobre las instituciones', 1974, 1, 'img/pequenas.jpg', 1),
	(4, 'Alto en la torre', 1975, 1, 'img/altaEnLaTorre.jpg', 4),
	(5, 'Há Sido', 1975, 1, 'img/haSido.jpg', 1),
	(6, 'Adios Sui Generis I', 1975, 1, 'img/AdiosI.jpg', 2),
	(7, 'Adios Sui Generis II', 1975, 1, 'img/AdiosII.jpg', 2),
	(8, 'Adios Sui Generis III', 1996, 1, 'img/AdiosIII.jpg', 2),
	(9, 'Sinfonías para adolescentes', 2000, 1, 'img/sinfonia.jpg', 1),
	(10, 'Si - Detrás de las paredes', 2001, 1, 'img/detras.jpg', 2),
	(12, 'Rasguña Las Piedras', 1999, 1, 'img/rasgunia.jpg', 3),
	(13, 'Obras Cumbres', 2020, 1, 'img/obras.jpg', 3),
	(14, 'PorSuiGieco', 1976, 2, 'img/porsuigieco.jpg', 1),
	(15, 'La máquina de hacer pájaros', 1976, 3, 'img/maquina.jpg', 1),
	(16, 'Películas', 1977, 3, 'img/pelicula.jpg', 1),
	(17, 'Billy Bond and The Jets', 1978, 4, 'img/billy.jpg', 1),
	(18, 'Serú Girán', 1978, 5, 'img/seru.jpg', 1),
	(19, 'La grasa de las capitales', 1979, 5, 'img/grasa.jpg', 1),
	(20, 'Bicicleta', 1980, 5, 'img/bicicleta.jpg', 1),
	(21, 'Peperina', 1981, 5, 'img/peperina.jpg', 1),
	(22, 'No llores por mí, Argentina', 1982, 5, 'img/noLLores.jpg', 2),
	(23, 'Serú \'92', 1992, 5, 'img/seru92.jpg', 1),
	(24, 'En Vivo I', 1993, 5, 'img/envivo.jpg', 2),
	(25, 'En Vivo II', 1993, 5, 'img/envivo.jpg', 2),
	(26, 'Yo no quiero volverme tan loco', 2000, 5, 'img/yoNoQuiero.jpg', 2),
	(27, 'Grandes Éxitos', 2009, 5, 'img/grandesExistos.jpg', 3),
	(28, 'Oro', 2009, 5, 'img/oro.jpg', 3),
	(29, 'Música del Alma', 1980, 6, 'img/musicaAlma.jpg', 2),
	(30, 'Yendo de la cama al living', 1982, 6, 'img/yendo.jpg', 1),
	(31, 'Clics modernos', 1983, 6, 'img/clicsModernos.jpg', 1),
	(32, 'Piano bar', 1984, 6, 'img/pianoBar.jpg', 1),
	(33, 'Tango', 1986, 6, 'img/tango.jpg', 1),
	(34, 'Parte de la religión', 1987, 6, 'img/parte.jpg', 1),
	(35, 'Cómo conseguir chicas', 1989, 6, 'img/chicas.jpg', 1),
	(36, 'Filosofía barata y zapatos de goma', 1990, 6, 'img/filosofia.jpg', 1),
	(37, 'Radio Pinti', 1991, 6, 'img/pinti.jpg', 1),
	(38, 'Tango 4', 1991, 6, 'img/tango4.jpg', 1),
	(39, 'La hija de la lágrima', 1994, 6, 'img/laHija.jpg', 1),
	(40, 'Estaba en llamas cuando me acosté', 1995, 6, 'img/estaba.jpg', 2),
	(41, 'Hello! MTV Unplugged', 1995, 6, 'img/hello.jpg', 2),
	(42, 'Say no More', 1996, 6, 'img/sayNoMore.jpg', 1),
	(43, 'Alta fidelidad con Mercedes Sosa', 1997, 6, 'img/altaFidelidad.jpg', 1),
	(44, 'El aguante', 1998, 6, 'img/aguante.jpg', 1),
	(45, 'Demasiado ego', 1999, 6, 'img/demasiado.jpg', 2),
	(46, 'Influencia', 2002, 6, 'img/influencia.jpg', 1),
	(47, 'Rock and Roll YO', 2003, 6, 'img/rockAndRollYo.jpg', 1),
	(48, 'Kill Gil', 2006, 6, 'img/killGil.jpg', 1),
	(49, 'El Concierto Subacuático', 2009, 6, 'img/concierto.jpg', 2),
	(50, '60x60', 2011, 6, 'img/60x60.jpg', 2),
	(51, 'Random', 2017, 6, 'img/random.jpg', 1);

-- Volcando estructura para tabla charly.tipo_cd
CREATE TABLE IF NOT EXISTS `tipo_cd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  KEY `Índice 1` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

-- Volcando datos para la tabla charly.tipo_cd: ~4 rows (aproximadamente)
INSERT INTO `tipo_cd` (`id`, `nombre`) VALUES
	(1, 'Grabado en Estudio'),
	(2, 'En Vivo'),
	(3, 'Compilados'),
	(4, 'EP');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
