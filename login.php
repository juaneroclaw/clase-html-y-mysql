<?php

include "__conexion_api.php";

function informe(&$cuerpo, $parametros, $campos, $informe, $cast){
	              if ($campos == '') {$campos = '*';}
              	  $query_cliente = "select $campos from api.f_informe('$parametros','$informe') as $cast";
              	  $result = pg_query($query_cliente);
              	  $cuerpo = pg_fetch_all($result);

              	  
}		



function informeProductos(&$cuerpo, $parametros, $campos, $informe, $cast){
	              if ($campos == '') {$campos = '*';}
              	  $query_cliente = "select $campos from api.f_informe('$parametros','$informe') as $cast";
              	  
              	  $result = pg_query($query_cliente);
              	  while ($row = pg_fetch_array($result, NULL, PGSQL_ASSOC))               	  
		          {if (base64_decode($row['detalle'])) 
					 { $row['detalle'] = base64_decode($row['detalle']);}
				 //  if (base64_decode($row['nombre'])) 
				 //	 { $row['nombre'] = base64_decode($row['nombre']);}
				   $cuerpo[] = $row;	
				  } 
				  	
}		


function cast($informe, $Cast){
	              $query_cliente = "select casteo from api.f_informe_casteo(cast('" . $informe . "Cast' as varchar)) as (casteo varchar)";
	              $casteo = $Cast;
              	  $result = pg_query($query_cliente);
              	  while ($row = pg_fetch_array($result, NULL, PGSQL_ASSOC)) 
		          {if ($row["casteo"] != '')
					  {$casteo = $row["casteo"];}
				  }              
              	  return $casteo;
}		

function venta(&$cuerpo, $parametros, $venta){

	              $parametros = $parametros . 'jsonCompraWeb=' . $venta . '&';
	              $query_cliente = "select api.carga_venta_creadores('$parametros')";
	              //
	              //echo $parametros;
              	  $result = pg_query($query_cliente);
		          $cuerpo = pg_fetch_all($result); 
}		

function test(&$cuerpo){
	              $query_cliente = "select mensaje FROM api.test()";
              	  $result = pg_query($query_cliente);
		          $cuerpo = pg_fetch_all($result); 
}



$CastProductos    = "(id integer, cod varchar,nombre varchar,copete varchar,detalle varchar, marca varchar,rubro varchar, subrub1 varchar, subrub2 varchar, stock numeric, precio numeric)";
$CastPact         = "(id integer, cod varchar,nombre varchar,copete varchar,detalle varchar, marca varchar,rubro varchar, subrub1 varchar, subrub2 varchar, stock numeric, precio numeric)";
$CastPStock       = "(idart integer, stock numeric)";
$CastClientes     = "(idpad integer, nomyap varchar, email varchar, clave integer, codvarpre varchar, tienectacte varchar, codiva varchar)";
$CastClientesExt  = "(nomyap varchar, email varchar, clave varchar, codvarpre varchar, tienectacte varchar, codiva varchar)";
$CastRubros       = "(cod varchar, nombre varchar, hijos integer)";
$CastPrecios      = "(id integer, precio numeric)";
$CastDtoCliRub    = "(id_padron integer, descuento varchar, rubro varchar, subrubro1 varchar, subrubro2 varchar)";

if ($url[1] == 'info')
{ $urlBase = 'https://hosting.abaco-informatica.com.ar/e_commerce/(proveedor)/(cliente)';
	
  echo '<h1>Descripcion de servicios e_commerce</h1>';
  echo 'Los servicios están implementados por medio de cubos del sistema de gestión, los cubos deben tener nombres específicos y devolver una estructura de datos determinada que debe seguir las siguientes reglas:';
  echo "<h2>Cast estandar</h2>";
  echo "Castear es indicar campo a campo de que tipo es, los tipos son integer, numeric y varchar, y deben coincidir totalmente con el cubo en cuestión.";
  echo "La especificación de un casteo es una cadena entre paréntesis con pares 'campo tipo' separados por coma. Ej. (id integer, nombre varchar, precio numeric)";
  echo "&nbspEs la definicion de los campos que tiene que devolver el cubo en cuestión. Deben castearse explícitamente en el cubo y respetarse los nombres." . '<br>';
  echo "<h2>Cubo de casteo</h2>";
  echo "&nbspEl cubo de casteo es un cubo que sólo devuelve un campo 'casteo' tipo varchar y tiene la cadena de casteo a aplicar sobre otro cubo". '<br>';
  echo "&nbspEsto permite usar el mismo servicio con campos diferentes a los indicados en Cast estandar o bien generar servicios nuevos en formato 'libre'". '<br>';
  echo "&nbspSe asocia a otro cubo y el codigo debe ser el del cubo original + 'Cast'. Ejemplo: XCom.ProductosCast". '<br>';
  echo "&nbspSELECT CAST(.... AS VARCHAR) AS CASTEO". '<br>';
  echo "&nbspEjemplo: SELECT CAST('(dato1 integer, dato2 varchar)' AS VARCHAR) AS CASTEO". '<br>';
  echo "&nbspSi el verdadero cubo devolverá dos campos, dato1 y dato2 , el primero entero y el segundo un texto." . '<br>';
  
  echo '<h1>Servicios implementados:</h1>';
  echo '<h2>Productos:</h2>';
  echo '&nbspCodigo Cubo: XCom.Productos<br>';
  echo "&nbspCast estandar: $CastProductos" . '<br>';
  echo "&nbsp$urlBase/productos". '<br>';
  echo '<h2>Productos actualizados a partir de fecha base:</h2>';
  echo '&nbspCodigo Cubo: XCom.Pact'. '<br>';
  echo "&nbspCast estandar: $CastPact". '<br>';
  echo "&nbspURL: hosting.abaco-informatica.com.ar/e_commerce/(proveedor)/(cliente)/productosAct/(fecha)>". '<br>';
  echo "&nbspLa fecha debe tener el formato AAAAMMDD o AAAA-MM-DD>". '<br>';
  echo '<h2>Stock:</h2>';
  echo '&nbspCodigo Cubo: XCom.PStock'. '<br>';
  echo "&nbspCast estandar: $CastPStock". '<br>';
  echo "&nbsp$urlBase/stock". '<br>';
  echo '<h2>Stock de un articulo:</h2>';
  echo '&nbspCodigo Cubo: XCom.PStock'. '<br>';
  echo "&nbspCast estandar: $CastPStock". '<br>';
  echo "&nbsp$urlBase/stock/(id de articulo)". '<br>';
  echo '<h2>Precios:</h2>';
  echo '&nbspCodigo Cubo: XCom.Precios'. '<br>';
  echo "&nbspCast estandar: $CastPrecios". '<br>';
  echo "&nbsp$urlBase/precios/(variable de precios)". '<br>';
  echo "El campo variable de precios es el codigo de la variable de precios en el sistema de gestión". '<br>';
  echo "El cubo debe tener el parámetro pVariable para recibir el valor de la variable de precios y hacer los cálculos". '<br>';
  echo '<h2>Descuento por cliente - rubro (de artículos):</h2>';
  echo '&nbspCodigo Cubo: XCom.DtoCliRub'. '<br>';
  echo "&nbspCast estandar: $CastDtoCliRub". '<br>';
  echo "&nbsp$urlBase/dtoclirub". '<br>';
  echo '<h2>Clientes:</h2>';
  echo '&nbspCodigo Cubo: XCom.clientes'. '<br>';
  echo "&nbspCast estandar: $CastClientes". '<br>';
  echo "&nbsp$urlBase/clientes". '<br>';
  echo '<h2>Clientes extendido:</h2>';
  echo '&nbspCodigo Cubo: XCom.clientesExt'. '<br>';
  echo "&nbspCast estandar: $CastClientesExt". '<br>';
  echo "&nbsp$urlBase/clientesExt". '<br>';
  echo '<h2>Rubros:</h2>';
  echo '&nbspCodigo Cubo: XCom.rubros'. '<br>';
  echo "&nbspCast estandar: $CastRubros". '<br>';
  echo "&nbsp$urlBase/rubros". '<br>';
  echo '<h2>Consulta libre:</h2>';
  echo '&nbspSe puede armar una consulta libre creando el cubo y el cubo de casteo en el sistema de gestión. No puede llevar parámetros extras.'. '<br>';
  echo '&nbspCodigo Cubo: XCom.(codigodelcubo)'. '<br>';
  echo "&nbspCast estandar: No hay casteo estandar, por lo cual hay que crear también un cubo de casteo". '<br>';
  echo "&nbsp$urlBase/libre/(codigodelcubo)". '<br>';
  echo '<h2>Consulta libre con parametros:</h2>';
  echo '&nbspSe puede armar una consulta libre con parámetros agregando los parámetros y su valor en la url luego de la urlBase, en una cadena parametro=valor entre caracteres &<br/>';
  echo '&nbspLos parámetros deben ser identicos a los que use el cubo y deben estar precedidos por :p y separados por ; ' . '<br>';
  echo '&nbspNo va la palabra libre en esta url' . '<br>';
  echo '&nbspCodigo Cubo: XCom.(codigodelcubo)'. '<br>';
  echo "&nbspCast estandar: No hay casteo estandar, por lo cual hay que crear también un cubo de casteo (sin parámetros)". '<br>';
  echo "&nbsp$urlBase/&:pParametro1=<valor1>;:pParametro2=<valor2>;...&/(codigodelcubo)". '<br>';
  echo '<h2>Parametros de la URL:</h2>';
  echo '&nbsp(proveedor) es el nombre del proveedor del sitio de e_commerce' . '<br>';
  echo '&nbsp(cliente) es el nombre de la configuracion del cliente en ábaco' . '<br>';
  
    
  exit;
 } 
 
 
switch ($url[3])
{ case "test":
	{ test($cuerpo);
	  break;
	}
  case "productos":
	{ 
	  $cast = cast('XCom.Productos', $CastProductos);
	  informeProductos($cuerpo, $parametros, '*', 'XCom.Productos', $cast);
	  break;
	}
  case "productos64":
	{ $cast = cast('XCom.Productos', $CastProductos);
	  informeProductos($cuerpo, $parametros, '*', 'XCom.Productos', $cast);
	  break;
	}
  case "productosAct":
	{ $cast = cast('XCom.Pact', $CastPact);
	  $parametros = $parametros . ":pFechaBase=" . $url[4] . "&";
	  informe($cuerpo, $parametros, '*', 'XCom.Pact', $cast);
	  break;
	}	
	
  case "stock":
	{ if (count($url) > 4)
  	     {$parametros = $parametros . ":pIdArtStock=" . $url[4] . "&";}
  	  else   
  	     {$parametros = $parametros . ":pIdArtStock=-1" . "&";}
	  $cast = cast('XCom.PStock', $CastPStock);
	  informe($cuerpo, $parametros, '*', 'XCom.PStock', $cast);
	  break;
	}	
  case "precios":
	{ $parametros = $parametros . ":pVariable=" . $url[4] . "&";
	  //echo $parametros;
	  $cast = cast('XCom.Precios', $CastPrecios);
	  if (count($url) > 5)
	     {$parametros = $parametros . "pFechaBase=" . $url[5] . "&";}
	  informe($cuerpo, $parametros, '*', 'XCom.Precios', $cast);
	  break;
	}

 case "clientes":
	{ //echo $parametros;
	  $cast = cast('XCom.Clientes', $CastClientes);
	  informe($cuerpo, $parametros, '*', 'XCom.Clientes', $cast);
	  break;
	}
	
 case "clientesExt":
	{ $cast = cast('XCom.clientesExt', $CastClientesExt);
	  informe($cuerpo, $parametros, '*', 'XCom.clientesExt', $cast);
	  break;
	}
 case "venta":
    { if (isset($GLOBALS))
	  {  $query_post = file_get_contents('php://input');
  	     venta($cuerpo, $parametros, $query_post);
  	     break;
      }
	}
	
 case "rubros":
	{ $cast = cast('XCom.Rubros', $CastRubros);
	  if (count($url) > 4)
  	     {$parametros = $parametros . "pNivel=" . $url[4] . "&";}
  	  else   
  	     {$parametros = $parametros . "pNivel=-1" . "&";}
  	  //echo $parametros . ' xxx ' . ;
	  informe($cuerpo, $parametros, '*', 'XCom.Rubros', $cast);
	  break;
	}
	
 case "dtoclirub":
	{ $cast = cast('XCom.DtoCliRub', $CastDtoCliRub);
	  informe($cuerpo, $parametros, '*', 'XCom.DtoCliRub', $cast);
	  break;
	}
	
 case "libre":
	{ $cast = cast('XCom.' . $url[4], '');
	  informe($cuerpo, $parametros, '*', 'XCom.' . $url[4], $cast);
	  break;
	}

 default: 
    { if ($url[3][0] == "&")
      { $CodigoCubo = 'XCom.' . $url[4];
	    $cast = cast($CodigoCubo,'');
	    $parametros = $parametros . ';' . $url[3];
	    informe($cuerpo, $parametros, '*', $CodigoCubo,  $cast);
      }
    }
 }

$tiporta = 'json';
 

if ($tiporta == 'json') 
  {echo json_encode($cuerpo);}
  elseif ($tiporta == 'array') {echo print_r($cuerpo);}
   elseif ($tiporta == 'plano') {echo $cuerpo;}
?>