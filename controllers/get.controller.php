<?php


require_once "models/discografia.php";
require_once "controller.php";

class GetController extends Controller{


// ==================
// Get Discografia
// ==================
public function getDiscografias(){
    $discografia= new Discografia($this->getDbm());
    $discografia->getDiscografias();  
    if ($discografia->getError() == 0){
        $json = array(
            "STATUS"=> "EXITO",
            "CODIGO"=>"200",
            "CHARLY"=> $discografia->getlistaDiscografia()
        );  
    }else{
        $json = array(
            "STATUS"=> "FRACASO",
            "CODIGO"=>"200",
            "MENSAJE"=> $discografia->getMensaje()
        );
    }
    $rest = new rest(200);
    return json_encode($json, http_response_code($json['CODIGO']));
}

// ==================
// Get Barrio por nombre
// ==================
public function getDiscografia($datos){
    $discografia= new Discografia($this->getDbm());
    $discografia->setIdBanda($datos[0]);
    $discografia->setIdTipoCd($datos[1]);
    $discografia->getDiscografiaPorFiltros();  
    if ($discografia->getError() == 0){
        $json = array(
            "STATUS"=> "EXITO",
            "CODIGO"=>"200",
            "CHARLY"=> $discografia->getlistaDiscografia()
        );  
    }else{
        $json = array(
            "STATUS"=> "FRACASO",
            "CODIGO"=>"200",
            "MENSAJE"=> $discografia->getMensaje(),
            'ENTRO'=>'hola'
        );
    }
    $rest = new rest(200);
    return json_encode($json, http_response_code($json['CODIGO']));
}

// ==================
// Get Bandas
// ==================
public function getBandas(){
    $bandas= new Discografia($this->getDbm());
    $bandas->getBanda();  
    if ($bandas->getError() == 0){
        $json = array(
            "STATUS"=> "EXITO",
            "CODIGO"=>"200",
            "CHARLY"=> $bandas->getlistaBanda()
        );  
    }else{
        $json = array(
            "STATUS"=> "FRACASO",
            "CODIGO"=>"200",
            "MENSAJE"=> $bandas->getMensaje()
        );
    }
    $rest = new rest(200);
    return json_encode($json, http_response_code($json['CODIGO']));
}

// ==================
// Get Bandas
// ==================
public function getTipos(){
    $tipos= new Discografia($this->getDbm());
    $tipos->getTipos();
    if ($tipos->getError() == 0){
        $json = array(
            "STATUS"=> "EXITO",
            "CODIGO"=>"200",
            "CHARLY"=> $tipos->getlistaTipos()
        );  
    }else{
        $json = array(
            "STATUS"=> "FRACASO",
            "CODIGO"=>"200",
            "MENSAJE"=> $tipos->getMensaje()
        );
    }
    $rest = new rest(200);
    return json_encode($json, http_response_code($json['CODIGO']));
}

// ==================
// Avistajes
// ==================
public function getAvistajes(){
    $usuario = new Usuario($this->getDbm());
   // $usuario->setToken($token);
    $usuario->activarUsuario();  
    if ($usuario->getError() == 0){
        $json = array(
            "STATUS"=> "EXITO",
            "CODIGO"=>"200",
            "MENSAJE"=> "Activacion exitosa"
        );  
    }else{
        $json = array(
            "STATUS"=> "FRACASO",
            "CODIGO"=>"200",
            "MENSAJE"=> $usuario->getMensaje()
        );
    }
    $rest = new rest(200);
    return json_encode($json, http_response_code($json['CODIGO']));
}
        
}