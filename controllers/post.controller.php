<?php

require_once "models/usuario.php";
require_once "vendor/autoload.php";
require_once "controller.php";

require_once "models/db_manager.php";
use Conexion\db_manager;

class PostController extends Controller{

    // ==================
    // Registro de Usuario
    // ==================
    public function registrarUsuario($nombre, $nick, $email, $password){   
        if ($email == '' or $password == '' or $nick == '' or $nombre == '') {
            $json = array(
                "STATUS"=> "FRACASO",
                "CODIGO"=>"200",
                "MENSAJE"=> "Todos los campos son requeridos",
            );
        } else {
            $usuario = new Usuario($this->getDbm());
            $usuario->setNombre($nombre);
            $usuario->setNick($nick);
            $usuario->setEmail($email);
            $usuario->setPasswordHash($password);
            $response = $usuario->registrarUsuario();
            if ($response->getError()== 0) {
                $json = array(
                    "STATUS"=> "EXITO",
                    "CODIGO"=>"200",
                    "MENSAJE"=> "Cuenta creada correctamente, se envio mail para activarla"
                    // inicio session con password
                );
            } else {
                $json = array(
                    "STATUS"=> "FRACASO",
                    "CODIGO"=>"200",
                    "MENSAJE"=> $usuario->getMensaje()
                );
            }
        }
        $rest = new rest(200);
        return json_encode($json, http_response_code($json['CODIGO']));
    }

    // ==================
    // Forget Password
    // ==================
    public function forgetPassword($email){   
        if ($email == '') {
            $json = array(
                "STATUS"=> "FRACASO",
                "CODIGO"=>"200",
                "MENSAJE"=> "Email no puede ser vacio",
            );
        } else {
            $usuario = new Usuario($this->getDbm());
            $usuario->setEmail($email);
            $usuario->forgetPassword();
            if ($usuario->getError()== 0) {
                $json = array(
                    "STATUS"=> "EXITO",
                    "CODIGO"=>"200",
                    "MENSAJE"=> "Se envio un mail para restablecer la clave"
                    // inicio session con password
                );
            } else {
                $json = array(
                    "STATUS"=> "FRACASO",
                    "CODIGO"=>"200",
                    "MENSAJE"=> $usuario->getMensaje()
                );
            }
        }
        $rest = new rest(200);
        return json_encode($json, http_response_code($json['CODIGO']));
    }
  
    // ==================
    // Reset Password
    // ==================
    public function resetPassword($token, $newpass){   
        if ($token == '' || $newpass == '') {
            $json = array(
                "STATUS"=> "FRACASO",
                "CODIGO"=>"200",
                "MENSAJE"=> "Todos los datos son requeridos",
            );
        } else {
            $usuario = new Usuario($this->getDbm());
            $usuario->setToken($token);
            $usuario->resetPassword($newpass);
            if ($usuario->getError()== 0) {
                $json = array(
                    "STATUS"=> "EXITO",
                    "CODIGO"=>"200",
                    "MENSAJE"=> "Clave actualizada"
                );
            } else {
                $json = array(
                    "STATUS"=> "FRACASO",
                    "CODIGO"=>"200",
                    "MENSAJE"=> $usuario->getMensaje()
                );
            }
        }
        $rest = new rest(200);
        return json_encode($json, http_response_code($json['CODIGO']));
    }
  
    // ============================
    // Actualizar Datos de Usuario
    // ============================
    public function actualizarDatos($token, $newNick, $newNombre){
        $usuario = new Usuario($this->getDbm());
        $usuario->setToken($token);
        $usuario->actualizarDatos($newNick, $newNombre);  
        if ($usuario->getError() != 0){
                $json = array(
                    "STATUS"=> "FRACASO",
                    "CODIGO"=>"200",
                    "MENSAJE"=> $usuario->getMensaje()
                );            
        }else{
            $json = array(
                "STATUS"=> "EXITO",
                "CODIGO"=>"200",
                "MENSAJE"=> "Datos actualizados"
            ); 
        };         
        $rest = new rest(200);
        return json_encode($json, http_response_code($json['CODIGO']));  
    }    

        // ==================
    // Login de Usuario
    // ==================
    public function loginUsuario($email, $password){
        $usuario = new Usuario($this->getDbm());
        $usuario->setEmail($email);
        $usuario->loginUsuario($password);  
        if ($usuario->getError() != 0){
                // no existe usuario
                $json = array(
                    "STATUS"=> "FRACASO",
                    "CODIGO"=>"200",
                    "MENSAJE"=> $usuario->getMensaje()
                );            
        }else{
            $json = array(
                "STATUS"=> "EXITO",
                "CODIGO"=>"200",
                "MENSAJE"=> "Login exitoso",
                "RESPUESTA"=> [
                    "id"=> $usuario->getId(),
                    "Email"=> $usuario->getEmail(),
                    "Token"=> $usuario->getToken(),
                    "Nombre"=>$usuario->getNombre(),
                    "Nick"=>$usuario->getNick()
                ]
            ); 
        };         
        $rest = new rest(200);
        return json_encode($json, http_response_code($json['CODIGO']));  
    }    
    // ==================
    // Add Barrio
    // ==================
    public function addBarrio($token, $idBar, $idCiu, $barCar, $barNro, $barNom, $barSob, $barOrd, $barAbr, $barCod, $barSag){   
        $barrio = new Barrio($this->getDbm());
        $usuario = new Usuario($this->getDbm());
        $usuario->setToken($token);
        $usuario->validarUsuario();
        if ($usuario->getError()== 0) {
            $barrio->addBarrio();
            if ($barrio->getError() == 0)
            {
                $json = array(
                    "STATUS"=> "EXITO",
                    "CODIGO"=>"200",
                    "MENSAJE"=> "Barrio agregado"
                );
            } else {
                $json = array(
                "STATUS"=> "FRACASO",
                "CODIGO"=>"200",
                "MENSAJE"=> $barrio->getMensaje()
                );
            }
        } else {
            $json = array(
                "STATUS"=> "FRACASO",
                "CODIGO"=>"200",
                "MENSAJE"=> $barrio->getMensaje()
            );
        }
        $rest = new rest(200);
        return json_encode($json, http_response_code($json['CODIGO']));
    }
    
}