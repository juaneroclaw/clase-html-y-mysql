<?php

require_once "models/db_manager.php";
require_once "routes/services/rest.php";

class Controller{

    private $dbm;
    private $ambiente;

    public function configuraDB(){
        $param = array(
            'dbhost'=>"127.0.0.1:3306",
            'dbport'=>"3306",
            'dbname'=>"charly",
            'dbuser'=>"root",
            'dbpass'=>""
        );
        $conn = new db_manager();        
        $this->dbm = $conn->conectar($param);
        $this->ambiente = 'TEST';   // = 'PROD';
    }

    public function getDbm(){
        return $this->dbm;
    }

    public function getAmbiente(){
        return $this->ambiente;
    }

    function validar_requerido(string $texto): bool
    {
        return !(trim($texto) == '');
    }

    /**
     * Método que valida si es un número entero 
     * @param {string} - Número a validar
     * @return {bool}
     */
    function validar_entero(string $numero): bool
    {
        return (filter_var($numero, FILTER_VALIDATE_INT) === FALSE) ? False : True;
    }

    /**
     * Método que valida si el texto tiene un formato válido de E-Mail
     * @param {string} - Email
     * @return {bool}
     */
    function validar_email(string $texto): bool
    {
        return (filter_var($texto, FILTER_VALIDATE_EMAIL) === FALSE) ? False : True;
    }

}