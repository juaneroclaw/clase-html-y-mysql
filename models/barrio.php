<?php

require_once "DAO/barrioDAOImpl.php";
require_once "db_manager.php";

Class Barrio
{
    private $id;
    private $idBar;
    private $idCiu;
    private $nombreCiu;
    private $barCar;
    private $barNro;
    private $barNom;
    private $barSob;
    private $barOrd;
    private $barAbr;
    private $barCod;
    private $barSag;
    // dbmanager
    private $dbm;
    // lista de barrios en json
    private $listaBarrios;
    // respuesta
    private $error;
    private $mensaje;

    public function __construct($dbm)
    {
        $this->dbm = $dbm;
    }

    public function getBarrios(){
        $brrDAO = new BarrioDAOImpl($this->dbm);
        $brrDAO->getAllBarrios($this);
        return $this;
    }

    public function getBarrioPorNombre(){
        $brrDAO = new BarrioDAOImpl($this->dbm);
        $brrDAO->getBarrioPorNombre($this);
        return $this;
    }

    public function addBarrio(){
        $brrDAO = new BarrioDAOImpl($this->dbm);
        $brrDAO->addBarrio($this);
        return $this;
    }

       /**
     * Get the value of jsonBarrios
     */ 
    public function getlistaBarrios()
    {
        return $this->listaBarrios;
    }

    /**
     * Set the value of jsonBarrios
     *
     * @return  self
     */ 
    public function setlistaBarrios($listaBarrios)
    {
        $this->listaBarrios = $listaBarrios;
        return $this;
    }
    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of mensaje
     */ 
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * Set the value of mensaje
     *
     * @return  self
     */ 
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;

        return $this;
    }

    /**
     * Get the value of idCiu
     */ 
    public function getIdCiu()
    {
        return $this->idCiu;
    }

    /**
     * Set the value of idCiu
     *
     * @return  self
     */ 
    public function setIdCiu($idCiu)
    {
        $this->idCiu = $idCiu;

        return $this;
    }

    /**
     * Get the value of barCar
     */ 
    public function getBarCar()
    {
        return $this->barCar;
    }

    /**
     * Set the value of barCar
     *
     * @return  self
     */ 
    public function setBarCar($barCar)
    {
        $this->barCar = $barCar;

        return $this;
    }

    /**
     * Get the value of barNro
     */ 
    public function getBarNro()
    {
        return $this->barNro;
    }

    /**
     * Set the value of barNro
     *
     * @return  self
     */ 
    public function setBarNro($barNro)
    {
        $this->barNro = $barNro;

        return $this;
    }

    /**
     * Get the value of barNom
     */ 
    public function getBarNom()
    {
        return $this->barNom;
    }

    /**
     * Set the value of barNom
     *
     * @return  self
     */ 
    public function setBarNom($barNom)
    {
        $this->barNom = $barNom;

        return $this;
    }

    /**
     * Get the value of barSob
     */ 
    public function getBarSob()
    {
        return $this->barSob;
    }

    /**
     * Set the value of barSob
     *
     * @return  self
     */ 
    public function setBarSob($barSob)
    {
        $this->barSob = $barSob;

        return $this;
    }

    /**
     * Get the value of barOrd
     */ 
    public function getBarOrd()
    {
        return $this->barOrd;
    }

    /**
     * Set the value of barOrd
     *
     * @return  self
     */ 
    public function setBarOrd($barOrd)
    {
        $this->barOrd = $barOrd;

        return $this;
    }

    /**
     * Get the value of barAbr
     */ 
    public function getBarAbr()
    {
        return $this->barAbr;
    }

    /**
     * Set the value of barAbr
     *
     * @return  self
     */ 
    public function setBarAbr($barAbr)
    {
        $this->barAbr = $barAbr;

        return $this;
    }


    /**
     * Get the value of barCod
     */ 
    public function getBarCod()
    {
        return $this->barCod;
    }

    /**
     * Set the value of barCod
     *
     * @return  self
     */ 
    public function setBarCod($barCod)
    {
        $this->barCod = $barCod;

        return $this;
    }


    /**
     * Get the value of barSag
     */ 
    public function getBarSag()
    {
        return $this->barSag;
    }

    /**
     * Set the value of barSag
     *
     * @return  self
     */ 
    public function setBarSag($barSag)
    {
        $this->barSag = $barSag;

        return $this;
    }

    /**
     * Get the value of error
     */ 
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set the value of error
     *
     * @return  self
     */ 
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }


    /**
     * Get the value of nombreCiu
     */ 
    public function getNombreCiu()
    {
        return $this->nombreCiu;
    }

    /**
     * Set the value of nombreCiu
     *
     * @return  self
     */ 
    public function setNombreCiu($nombreCiu)
    {
        $this->nombreCiu = $nombreCiu;

        return $this;
    }

    /**
     * Get the value of idBar
     */ 
    public function getIdBar()
    {
        return $this->idBar;
    }

    /**
     * Set the value of idBar
     *
     * @return  self
     */ 
    public function setIdBar($idBar)
    {
        $this->idBar = $idBar;

        return $this;
    }
}