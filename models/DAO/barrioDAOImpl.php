<?php

require_once "barrioDAO.php";
require_once "models/db_manager.php";

class BarrioDAOImpl implements BarrioDAO
{
    private $dbm;
    private $ambiente;

    public function __construct($dbm, $ambiente = 'TEST')
    {
        $this->dbm = $dbm;
        $this->ambiente = $ambiente;
    }

    public function getAllBarrios($objBarrio){
        try{
            $sql = "SELECT * from analo83_tucanes.tblBarrios tuc 
            inner join analo83_tucanes.tblCiudad ciu on tuc.idCiu = ciu.idCiu";
            $stmt = $this->dbm->prepare($sql);
            $stmt->execute(); 
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($result) > 0) {
                $objBarrio->setlistaBarrios($result);
                $objBarrio->setError(0);
                $objBarrio->setMensaje('');
            }else{
                $objBarrio->setError(8);
                $objBarrio->setMensaje('No existen barrios');
            }
            return;
        } catch(PDOException $e) {
            $objBarrio->setError(-1);
            if ($this->ambiente == 'TEST'){
                $objBarrio->setMensaje($e->getMessage());
            }else{
                $objBarrio->setMensaje('Error interno');
            }
            return;
        }
    }

    public function getBarrioPorNombre($objBarrio)
    {
        try{
            $sql = "SELECT * from analo83_tucanes.tblBarrios bar 
            inner join analo83_tucanes.tblCiudad ciu on bar.idCiu = ciu.idCiu
            where bar.barNom = :barNom";
            $nomBar = $objBarrio->getbarNom();
            $nomBar = trim($nomBar);
            $stmt = $this->dbm->prepare($sql);
            $stmt->bindParam(':barNom', $nomBar);
            $stmt->execute(); 
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($result) > 0) {
                $objBarrio->setId((isset($result[0]["id"]) ? $result[0]["id"] : 0));
                $objBarrio->setidBar((isset($result[0]["idBar"]) ? $result[0]["idBar"] : 0));
                $objBarrio->setidCiu((isset($result[0]["idCiu"]) ? $result[0]["idCiu"] : 0));
                $objBarrio->setbarCar((isset($result[0]["barCar"]) ? $result[0]["barCar"] : 0));
                $objBarrio->setbarNro((isset($result[0]["barNro"]) ? $result[0]["barNro"] : 0));
                $objBarrio->setbarNom((isset($result[0]["barNom"]) ? $result[0]["barNom"] : ''));
                $objBarrio->setbarSob((isset($result[0]["barSob"]) ? $result[0]["barSob"] : ''));
                $objBarrio->setbarOrd((isset($result[0]["barOrd"]) ? $result[0]["barOrd"] : ''));
                $objBarrio->setbarAbr((isset($result[0]["barAbr"]) ? $result[0]["barAbr"] : ''));
                $objBarrio->setbarCod((isset($result[0]["barCod"]) ? $result[0]["barCod"] : ''));
                $objBarrio->setbarSag((isset($result[0]["barSag"]) ? $result[0]["barSag"] : ''));
                $objBarrio->setlistaBarrios((isset($result[0]) ? $result[0] : []));   // para la respuesta
                $objBarrio->setError(0);
                $objBarrio->setMensaje('');
            }else{
                $objBarrio->setError(8);
                $objBarrio->setMensaje('No existen barrios');
            }
            return;
        } catch(PDOException $e) {
            $objBarrio->setError(-1);
            if ($this->ambiente == 'TEST'){
                $objBarrio->setMensaje($e->getMessage());
            }else{
                $objBarrio->setMensaje('Error interno');
            }
            return;
        }
    }

    public function addBarrio($objBarrio){
        try{
            $sql = "INSERT INTO analo83_tucanes.tblBarrios
             (`idBar`, `idCiu`, `barCar`, `barNro`, `barNom`, `barSob`, `barOrd`, `barAbr`, `barCod`, `barSag`) 
            VALUES (:idBar, :idCiu, :barCar, :barNro, :barNom, :barSob, :barOrd, :barAbr, :barCod, :barSag);";
            $nomBar = $objBarrio->getbarNom();
            $nomBar = trim($nomBar);
            $stmt = $this->dbm->prepare($sql);
            $stmt->bindParam(':idBar', $objBarrio->getIdBar());
            $stmt->bindParam(':idCiu', $objBarrio->getIdCiu());
            $stmt->bindParam(':barCar', $objBarrio->getBarCar());
            $stmt->bindParam(':barNro', $objBarrio->getBarNro());
            $stmt->bindParam(':barNom', $objBarrio->getBarNom());
            $stmt->bindParam(':barSob', $objBarrio->getBarSob());
            $stmt->bindParam(':barOrd', $objBarrio->getBarOrd());
            $stmt->bindParam(':barAbr', $objBarrio->getBarAbr());
            $stmt->bindParam(':barCod', $objBarrio->getBarCod());
            $stmt->bindParam(':barSag', $objBarrio->getBarSag());
            $res = $stmt->execute(); 
            if ($res = 1) {
                $objBarrio->setError(0);
                $objBarrio->setMensaje('');
            }else{
                $objBarrio->setError(9);
                $objBarrio->setMensaje('No se pudo insertar');
            }
            return;
        } catch(PDOException $e) {
            $objBarrio->setError(-1);
            if ($this->ambiente == 'TEST'){
                $objBarrio->setMensaje($e->getMessage());
            }else{
                $objBarrio->setMensaje('Error interno');
            }
            return;
        }
    }

    public function updateBarrio($usuario){

    }
}