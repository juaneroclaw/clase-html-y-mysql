<?php

require_once "usuarioDAO.php";
require_once "models/db_manager.php";

define('TIMEOUT_REGISTRO','PT6H');

class UsuarioDAOImpl implements UsuarioDAO
{
    private $dbm;
    private $ambiente;

    public function __construct($dbm, $ambiente = 'TEST')
    {
        $this->dbm = $dbm;
        $this->ambiente = $ambiente;
    }

    public function registrarUsuario($objUsuario)
    {
        try {
            // verifico que no exista el email
            $sql = "SELECT * FROM analo83_tucanes.login WHERE email = :email";
            $stmt = $this->dbm->prepare($sql);
            $email = trim($objUsuario->getEmail());
            $stmt ->bindParam(':email', $email);
            $result = $stmt->execute();
            if ($result == 1) {
                $response = $stmt->fetchAll();
                if (count($response) >= 1){
                    $objUsuario->setError(1);
                    $objUsuario->setMensaje('Ya existe email');
                    return;
                }else{  //inserto el registro
                    $sql="INSERT INTO analo83_tucanes.login(id, name, email, nick, password, Estado, token, fecha_registro) 
                    VALUES(1, :name, :email, :nick, :pass, :estado, :token, CURRENT_TIMESTAMP)";
                    $stmt = $this->dbm->prepare($sql);
                    $nombre = trim($objUsuario->getNombre());
                    $stmt ->bindParam(':name', $nombre);
                    $email = trim($objUsuario->getEmail());
                    $stmt ->bindParam(':email', $email);
                    $nick = trim($objUsuario->getNick());
                    $stmt ->bindParam(':nick', $nick);
                    $pass = trim($objUsuario->getPassword());
                    $stmt ->bindParam(':pass', $pass);
                    $estado = trim($objUsuario->getEstado());
                    $stmt ->bindParam(':estado', $estado);
                    $token = trim($objUsuario->getToken());
                    $stmt ->bindParam(':token', $token);
                    $result = $stmt->execute();
                    if ($result == 1){
                        $objUsuario->setError(0);
                        $objUsuario->setMensaje('Cuenta registrada');
                        return;
                    }else{
                        $objUsuario->setError(2);
                        $objUsuario->setMensaje('No se pudo registrar la cuenta');
                        return;
                    }
                }
            }
            return;
        } catch(PDOException $e) {
            $objUsuario->setError(-1);
            $objUsuario->setMensaje($e->getMessage());
            return;
        }
    }

   // busca usuario por token
   public function getPorToken($objUsuario){
    try{
        $token = trim($objUsuario->getToken());
        $sql = "SELECT * from analo83_tucanes.login where token = :token";
        $stmt = $this->dbm->prepare($sql);
        $stmt->bindParam(':token', $token);
        $stmt->execute(); 
        $result = $stmt->fetchAll();
        if (count($result) > 0 and isset($result[0])) {
            $objUsuario->setNick($result[0]["nick"]);
            $objUsuario->setNombre($result[0]["name"]);
            $objUsuario->setPassword($result[0]["password"]);
            $objUsuario->setFecha_login($result[0]["fecha_login"]);
            $objUsuario->setFecha_registro($result[0]["fecha_registro"]);
            $objUsuario->setToken($result[0]["token"]);
            $objUsuario->setEstado($result[0]["Estado"]);
            $objUsuario->setError(0);
            $objUsuario->setMensaje('');
        }else{
            $objUsuario->setError(8);
            $objUsuario->setMensaje('No existe usuario');
        }
        return;
    } catch(PDOException $e) {
        $objUsuario->setError(-1);
        $objUsuario->setMensaje($e->getMessage());
        return;
    }
}    

    public function activarUsuario($objUsuario)
    {
        try {
            $sql = "UPDATE analo83_tucanes.login SET estado = 'A' where token = :token ";
            $stmt = $this->dbm->prepare($sql);
            $token = trim($objUsuario->getToken());
            $stmt ->bindParam(':token', $token);
            $result = $stmt->execute();
            if ($result == 1) {
                $response = $stmt->fetch(PDO::FETCH_ASSOC);
                $objUsuario->setError(0);
                $objUsuario->setMensaje('');
                return;
            }
            $objUsuario->setError(1);
            $objUsuario->setMensaje('No se pudo activar el usuario');
            return;
        } catch(PDOException $e) {
            $objUsuario->setError(-1);
            $objUsuario->setMensaje($e->getMessage());
            return;
        }
    }    

    // Obtiene datos de usario por email
    public function getUsuario($objUsuario)
    {
        try {
            $sql =  "SELECT * from analo83_tucanes.login where email = :email";
            $stmt = $this->dbm->prepare($sql);
            $email = trim($objUsuario->getEmail());              
            $stmt ->bindParam(':email', $email);
            $resQuery = $stmt->execute();
            if ($resQuery == 1){
                $result = $stmt->fetchAll();
                if (count($result) > 0 and isset($result[0])){
                    $objUsuario->setId($result[0]["id"]);
                    $objUsuario->setNick($result[0]["nick"]);
                    $objUsuario->setNombre($result[0]["name"]);
                    $objUsuario->setPassword($result[0]["password"]);
                    $objUsuario->setFecha_login($result[0]["fecha_login"]);
                    $objUsuario->setFecha_registro($result[0]["fecha_registro"]);
                    $objUsuario->setToken($result[0]["token"]);
                    $objUsuario->setEstado($result[0]["Estado"]);
                    $objUsuario->setError(0);
                    $objUsuario->setMensaje('');
                }else{
                    $objUsuario->setError(6);
                    $objUsuario->setMensaje('Usuario no existe');
                }
            }else{
                $objUsuario->setError(7);
                $objUsuario->setMensaje('Error el ejecutar consulta');
            }
            return;
        } catch(PDOException $e) {
            $objUsuario->setError(-1);
            $objUsuario->setMensaje($e->getMessage());
            return;
        }
    }

    public function actualizarPasswordRestablecer($usuario)
    {
        try {
            $sql = "UPDATE analo83_tucanes.login SET password = :clave where token = :token ";
            $stmt = $this->dbm->prepare($sql);
            $token = $usuario->getToken();
            $token = trim($token);
            $stmt ->bindParam(':token', $token);          
            $password = $usuario->getPassword();
            $password = trim($password);  
            $stmt ->bindParam(':clave', $password);
            $result = $stmt->execute();
            if ($result == 1) {
                $usuario->setError(0);
                return;               
            }
            $usuario->setError(1);
            $usuario->setMensaje("No se pudo restablecer la clave");
            return;
        } catch(PDOException $e) {
            $usuario->setError(-1);
            $usuario->setMensaje($e->getMessage());
            return;
        }
    }

    // actualizar el ṕerfil
    public function actualizarDatos($objUsuario)
    {
        try {
            $sql = "UPDATE analo83_tucanes.login SET nick = :newNick, name = :newNombre where token = :token";
            $stmt = $this->dbm->prepare($sql);
            $token = trim($objUsuario->getToken());
            $nick = trim($objUsuario->getNick());
            $nombre = trim($objUsuario->getNombre());
            $stmt ->bindParam(':token', $token);
            $stmt ->bindParam(':newNick', $nick);
            $stmt ->bindParam(':newNombre', $nombre);
            $result = $stmt->execute();
            if ($result == 1) {
                    $objUsuario->setError(0);
                    $objUsuario->setMensaje('');
            }else{
                $objUsuario->setError(9);
                $objUsuario->setMensaje('No se pudo actualizar');
            }
            return ;
        } catch(PDOException $e) {
            $objUsuario->setError(-1);
            $objUsuario->setMensaje($e->getMessage());
            return;
        }        
    }

    // graba el token
    public function grabaToken($objUsuario){
        try{
            $sql = "UPDATE analo83_tucanes.login SET token = :token, fecha_login = now() where email = :email";
            $stmt = $this->dbm->prepare($sql);
            $token = trim($objUsuario->getToken());
            $stmt ->bindParam(':token', $token);
            $email = trim($objUsuario->getEmail());
            $stmt->bindParam(':email', $email);
            $result = $stmt->execute();            
            if ($result == 1){
                $objUsuario->setError(0);
                $objUsuario->setMensaje('');                                
            }else{
                $objUsuario->setError(10);
                $objUsuario->setMensaje('No se pudo grabar el token');
            }
            return;
        } catch(PDOException $e) {
            $objUsuario->setError(-1);
            $objUsuario->setMensaje($e->getMessage());
            return;
        }
    }

    private function armArrayError($e){
        if ($this->ambiente = 'TEST'){
            $retorno = array(
                "ERROR"=> $e->getCode(),
                "MENSAJE"=>$e->getMessage()
            );
        } else {
            $retorno = array(
                "ERROR"=> 2,
                "MENSAJE"=>'Error de datos'
            );            
        }

        return $retorno;
    }
};