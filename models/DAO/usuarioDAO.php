<?php

interface UsuarioDAO{

    public function getUsuario($objUsuario);
    public function registrarUsuario($objUsuario);
    public function actualizarPasswordRestablecer($usuario);
    public function activarUsuario($objUsuario);
    public function actualizarDatos($objUsuario);
    public function grabaToken($objUsuario);
    public function getPorToken($objUsuario);
};