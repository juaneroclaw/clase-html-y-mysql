<?php

interface DiscografiaDAO{

    public function getAllDiscografia($objDisco);
    public function getDiscografiaPorFiltros($objDisco);
    public function getBandas($objDisco);
    public function getTipos($objDisco);
};