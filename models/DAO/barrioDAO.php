<?php

interface BarrioDAO{

    public function getAllBarrios($objBarrio);
    public function getBarrioPorNombre($objBarrio);
    public function addBarrio($objBarrio);
    public function updateBarrio($usuario);
};