<?php

require_once "discografiaDAO.php";
require_once "models/db_manager.php";

class DiscografiaDAOImpl implements DiscografiaDAO
{
    private $dbm;
    private $ambiente;

    public function __construct($dbm, $ambiente = 'TEST')
    {
        $this->dbm = $dbm;
        $this->ambiente = $ambiente;
    }

    public function getAllDiscografia($objDisco){
        try{
            $sql = "SELECT disc.* , ban.nombre as nombre_banda, cd.nombre as tipo_cd from charly.discografia disc 
            inner join charly.banda ban on disc.id_banda = ban.id
            inner join charly.tipo_cd cd on disc.id_tipo_cd = cd.id
            Order by disc.id ASC";
            $stmt = $this->dbm->prepare($sql);
            $stmt->execute(); 
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($result) > 0) {
                $objDisco->setlistaDiscografia($result);
                $objDisco->setError(0);
                $objDisco->setMensaje('');
            }else{
                $objDisco->setError(8);
                $objDisco->setMensaje('No existen discografias');
            }
            return;
        } catch(PDOException $e) {
            $objDisco->setError(-1);
            if ($this->ambiente == 'TEST'){
                $objDisco->setMensaje($e->getMessage());
            }else{
                $objDisco->setMensaje('Error interno');
            }
            return;
        }
    }

    public function getDiscografiaPorFiltros($objDisco)
    {
        try{
            $idBanda = $objDisco->getIdBanda();
            $idCd = $objDisco->getIdTipoCd();
            $idBanda = trim($idBanda);
            $idCd = trim($idCd);

            $sql = "SELECT disc.* , ban.nombre as nombre_banda, cd.nombre as tipo_cd from charly.discografia disc 
            inner join charly.banda ban on disc.id_banda = ban.id
            inner join charly.tipo_cd cd on disc.id_tipo_cd = cd.id ";
            if($idBanda != 0){
                $sql = $sql." where disc.id_banda = :id_banda " ;
            }
            if($idCd != 0 && $idBanda != 0 ){
                $sql = $sql." and disc.id_tipo_cd = :id_tipo_cd " ;
            }else{
                if($idCd != 0 && $idBanda == 0 ){
                    $sql = $sql." where disc.id_tipo_cd = :id_tipo_cd " ;
                }
            }
            $sql = $sql." Order by disc.id ASC";
            $stmt = $this->dbm->prepare($sql);
            if($idBanda != 0){
                $stmt->bindParam(':id_banda', $idBanda);
            }
            if($idCd != 0){
                $stmt->bindParam(':id_tipo_cd', $idCd);
            }
            
            $stmt->execute(); 
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($result) > 0) {
                $objDisco->setlistaDiscografia($result);
                $objDisco->setError(0);
                $objDisco->setMensaje('');
            }else{
                $objDisco->setError(8);
                $objDisco->setMensaje('No existen discografias');
            }
            return;
        } catch(PDOException $e) {
            $objDisco->setError(-1);
            if ($this->ambiente == 'TEST'){
                $objDisco->setMensaje($e->getMessage());
            }else{
                $objDisco->setMensaje('Error interno');
            }
            return;
        }
    }

    public function getBandas($objDisco){
        try{
            $sql = "SELECT * from charly.banda banda Order by banda.id ASC";
            $stmt = $this->dbm->prepare($sql);
            $stmt->execute(); 
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($result) > 0) {
                $objDisco->setlistaBanda($result);
                $objDisco->setError(0);
                $objDisco->setMensaje('');
            }else{
                $objDisco->setError(8);
                $objDisco->setMensaje('No existen Bandas');
            }
            return;
        } catch(PDOException $e) {
            $objDisco->setError(-1);
            if ($this->ambiente == 'TEST'){
                $objDisco->setMensaje($e->getMessage());
            }else{
                $objDisco->setMensaje('Error interno');
            }
            return;
        }
    }
    public function getTipos($objDisco){
        try{
            $sql = "SELECT * from charly.tipo_cd tipo Order by tipo.id ASC";
            $stmt = $this->dbm->prepare($sql);
            $stmt->execute(); 
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($result) > 0) {
                $objDisco->setlistaTipos($result);
                $objDisco->setError(0);
                $objDisco->setMensaje('');
            }else{
                $objDisco->setError(8);
                $objDisco->setMensaje('No existen Bandas');
            }
            return;
        } catch(PDOException $e) {
            $objDisco->setError(-1);
            if ($this->ambiente == 'TEST'){
                $objDisco->setMensaje($e->getMessage());
            }else{
                $objDisco->setMensaje('Error interno');
            }
            return;
        }
    }
}