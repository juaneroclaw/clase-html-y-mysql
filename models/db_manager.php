<?php


//namespace Conexion;
 
class db_manager {
	
	function conectar($parametros) {
try {
    $dbhost = $parametros['dbhost'];
    $dbport = $parametros['dbport'];
    $dbuser	= $parametros['dbuser'];    // database username
    $dbpass	= $parametros['dbpass'];    // database password
    $dbname	= $parametros['dbname'];    // database name
    //$dbh = new mysqli($dbhost,$dbuser,$dbpass,$dbname);
    $dsn = "mysql:host=" . $parametros['dbhost'] . " dbname=" . $parametros['dbname'] . " charset=UTF8";
    $dbh = new \PDO($dsn, $dbuser, $dbpass);
	$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
   // $dbh->query("SET CLIENT_ENCODING TO 'UTF8';");

    return $dbh;
} catch (PDOException $e){
    echo $e->getMessage();
}

		//if (!$dbh) {
		//	die("Connection failed: " . mysqli_connect_error());
		//}

		//if (!$dbh->set_charset("utf8")) {//asignamos la codificación comprobando que no falle
		//	die("Error cargando el conjunto de caracteres utf8");
		//	return $dbh;
		//}
	}
 
	function desconectar($dbh) {
		$dbh = null;
	}
}
 
?>