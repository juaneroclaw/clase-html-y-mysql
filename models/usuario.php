
<?php

require_once "DAO/usuarioDAOImpl.php";
require_once "db_manager.php";
require_once "vendor/autoload.php";
use Firebase\JWT\JWT;

use PHPMailer\PHPMailer\PHPMailer; 
use PHPMailer\PHPMailer\Exception;
/* Clase para tratar con excepciones y errores */
require 'vendor/phpmailer/phpmailer/src/Exception.php';
/* Clase PHPMailer */
require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
/*Clase SMTP necesaria para conectarte a un servidor SMTP */
require 'vendor/phpmailer/phpmailer/src/SMTP.php';

class Usuario
{
    private $id;
    private $nick;
    private $email;
    private $password;
    private $nombre;
    private $token;
    private $fecha_login;
    private $fecha_registro;
    private $estado;
    // dbmanager
    private $dbm;
    // para envio de mails
    private $urlPrincipal = "http://localhost/";
    // respuesta
    private $error;
    private $mensaje;

    public function __construct($dbm)
    {
        $this->dbm = $dbm;
    }
    
    // Registrar Usuario
    public function registrarUsuario(){
        $usrDAO = new UsuarioDAOImpl($this->dbm);
        $this->setEstado('P');   
        $this->setNuevoToken();
        $usrDAO->registrarUsuario($this);
        if ($this->getError() == 0){
            // envio el mail de activacion
            $envioMail = $this->enviarMailActivacion();
            if ($envioMail != 0){
                $this->setError(3);
                $this->setMensaje('Error en envio de email');
            }else{
                $this->setError(0);
                $this->setMensaje('Cuenta registrada');
            }
        }
        return $this;
    }

    /*==================================
    Activar usuario
    ====================================*/
    public function activarUsuario(){
        $usrDAO = new UsuarioDAOImpl($this->dbm);
        $usrDAO->getPorToken($this);
        if ($this->getError() == 0) {
            if ($this->getEstado() == 'P') {
                $fechareg = new DateTime($this->getFecha_registro());
                $dt = new DateTime();
                $fechareg->add(new DateInterval(TIMEOUT_REGISTRO));
                if ($fechareg >= $dt) {
                    // cambio el estado a Activado
                    $usrDAO->activarUsuario($this);
                    if ($this->getError() == 0) {
                        $this->setMensaje('');
                        return $this;
                    } else{ 
                        $this->setError(5);
                        $this->setMensaje('No se pudo activar el usuario');
                        return $this;
                    }
                } else {
                    $this->setError(4);
                    $this->setMensaje('Vencio el tiempo de activacion, registrese nuevamente');
                    return $this;
                }
            }
        }
        $this->setError(6);
        $this->setMensaje('Usuario inexistente o ya activado');
        return $this;
    }    

    // forget password
    public function forgetPassword(){
        if (empty($this->email)){
            $this->setError(1);
            $this->setMensaje('No tiene email');
        }
        $usrDAO = new UsuarioDAOImpl($this->dbm);
        $usrDAO->getUsuario($this);
        if ($this->getError() == 0){   // existe usuario
            // envio mail para restablecer la clave
            $envioMail = $this->enviarMailRestablecer();
            if ($envioMail != 0){
                $this->setError(3);
                $this->setMensaje('Error en envio de email');
            }else{
                $this->setError(0);
                $this->setMensaje('Se envio mail para restablecer la clave');
            }
        }else{
            $this->setError(1);
            $this->setMensaje($this->getMensaje());
        }
        return $this;
    }

    // reset password
    public function resetPassword($newpass){
        // si existe usuario respondo con datos y token de logueo
        $usrDAO = new UsuarioDAOImpl($this->dbm);
        $usrDAO->getPorToken($this);
        if ($this->getError() == 0){
            if ($this->getEstado() == 'A'){    // tiene que estar Aceptadoy con token vigente  
                $fechalog = new DateTime($this->getFecha_registro());
                $dt = new DateTime();
                $fechalog->add(new DateInterval(TIMEOUT_REGISTRO));
                if ($fechalog >= $dt) {
                    // encriptar password
                    $newpass = trim($newpass);
                    $this->setPasswordHash($newpass);
                    $usrDAO->actualizarPasswordRestablecer($this);
                }
            }
        }
        return $this;   	
    }   

    // Valida que el token sea valido (existe y no esta vencido)
    public function validarUsuario(){
        $usrDAO = new UsuarioDAOImpl($this->dbm);
        $usrDAO->getPorToken($this);
        if ($this->getError() == 0 && $this->getEstado() == 'A'){
            $fechalog = new DateTime($this->getFecha_login());
            $dt = new DateTime();
            $fechalog->add(new DateInterval(TIMEOUT_REGISTRO));
            if ($fechalog >= $dt) {
                $this->setError(0);
                $this->setMensaje('Usuario valido');
                return $this;
            }
            $this->setError(10);
            $this->setMensaje('Usuario no es valido');
        }
        return $this;
    }    

    // Actualizar Datos Usuario (nick y nombre)
    public function actualizarDatos($newNick, $newNombre){
        $usrDAO = new UsuarioDAOImpl($this->dbm);
        $usrDAO->getPorToken($this);
        if ($this->getError() == 0 && $this->getEstado() == 'A'){
            $fechalog = new DateTime($this->getFecha_login());
            $dt = new DateTime();
            $fechalog->add(new DateInterval(TIMEOUT_REGISTRO));
            if ($fechalog >= $dt) {
                $this->setNick($newNick);
                $this->setNombre($newNombre);
                $usrDAO->actualizarDatos($this);
            }
        }
        return $this;
    }    

    /*==================================
    Login de usuario
    ====================================*/
    public function loginUsuario($password){
        // si existe usuario respondo con true
        $usrDAO = new UsuarioDAOImpl($this->dbm);
        $usrDAO->getUsuario($this);
        if ($this->getError() == 0 && $this->getEstado() == 'A'){   // existe usuario y esta activado
            if (password_verify($password, $this->getPassword())){
                // grabo el token
                $this->setNuevoToken();
                $usrDAO->grabaToken($this);
                if ($this->getError() == 0){
                    $this->setError(0);
                    $this->setMensaje("");  
                }else{
                    $this->setError(1);
                    $this->setMensaje("No se pud autenticar usuario");                      
                }
            }else{
                $this->setError(1);
                $this->setMensaje("Usuario no existe o clave invalida");                 
            }     
        }else{
            $this->setError(1);
            $this->setMensaje("Usuario no existe o clave invalida"); 
        };
        return $this;
    } 

    public function getEmail(){
        return $this->email;
    }

    public function setId($id){
        $this->id = $id;
    }    

    public function getId(){
        return $this->id;
    }    

    public function getPassword(){
        return $this->password;
    }   

    public function setEmail($email){
        $this->email = trim($email);
    }

    public function setPassword($password){
        $this->password = trim($password);
    }

    public function setPasswordHash($password){
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }

    public function verificarPassword($password){
        return password_verify($password, $this->password);
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function setError($err){
        $this->error = $err;
    }

    public function getError(){
        return $this->error;
    }

    public function setMensaje($msg){
        $this->mensaje = $msg;
    }

    public function getMensaje(){
        return $this->mensaje;
    }

    public function setNick($nick){
        $this->nick = $nick;
    }

    public function getNick(){
        return $this->nick;
    }

    public function setFecha_login($fecha_ult_login){
        $this->fecha_login = $fecha_ult_login;
    }

    public function getFecha_login(){
        return $this->fecha_login;
    }

    public function setFecha_registro($fecha_registro){
        $this->fecha_registro = $fecha_registro;
    }

    public function getFecha_registro(){
        return $this->fecha_registro;
    }

    public function setEstado($estado){
        $this->estado = $estado;
    }

    public function getEstado(){
        return $this->estado;
    }

    public function setNuevoToken(){
        /*======================================
        Genera token 
        ========================================*/
        $time = time();
        $token = array(
            "iat"=> $time, // tiempo que inicia el token
            "exp"=> $time + 21600,  // solo se usa para armar el token
            "data"=> [
                "id"=>$this->id,
                "email"=> $this->email
            ]
        );
        $this->token = trim((JWT::encode($token, "TuCaNeSeNMiJuJuY", 'HS256')));
        return true;
    }

    public function setToken($token){
        $this->token = $token;
    }

    public function getToken(){
        return $this->token;
    }
      // ====================
    // Enviar mail Registro
    // ====================
    private function enviarMailActivacion(){
        try {            
            // url de ofivirtual + ruta de confirmacion
            $url = $this->urlPrincipal.'/activacion/?activacion=';
            // cargamos el template html y le pasamos los datos
            $mensaje =file_get_contents("asset/email/confirmation.html");
            $mensaje = str_replace("%KKK001%", $this->getEmail(), $mensaje);
            $mensaje = str_replace("%KKK002%", '', $mensaje);
            $mensaje = str_replace("KKK003", $url.$this->getToken(), $mensaje);
            $mensaje = str_replace("%KKK004%", $url.$this->getToken(), $mensaje);

            $phpmailer = new PHPMailer(TRUE);            
            $phpmailer->isSMTP();
            // luego cambiar por el smtp de abaco
            $phpmailer->Host = 'smtp.gmail.com';
            $phpmailer->SMTPAuth = true;
            $phpmailer->Port = 465;
            $phpmailer->SMTPSecure = 'ssl';
            $phpmailer->SMTPAuth   = true;
            $phpmailer->Username = 'roberto.arturo.salinas@gmail.com';
            $phpmailer->Password = 'jtbmeerztpiakrxv';
            $phpmailer->setFrom('roberto.arturo.salinas@gmail.com', 'Tucanes en mi Jujuy');

            $phpmailer->addAddress($this->getEmail(), $this->getNombre());
            $phpmailer->addReplyTo('rsalinas@telpin.com.ar', 'Tucanes en mi Jujuy');
            //$phpmailer->addEmbeddedImage('asset/email/abaco', 'abaco', 'abaco', 'base64', 'image/jpg');
            $phpmailer->WordWrap = 50;
            $phpmailer->isHTML(true);
            
            $phpmailer->Body = "Confirmacion de Registro en Tucanes en mi Jujuy";
            $phpmailer->Subject = "Confirmacion de Registro en Tucanes en mi Jujuy";
            $phpmailer->AltBody = "Confirmacion de Registro en Tucanes en mi Jujuy";
            $phpmailer->msgHTML($mensaje);
            if (!$phpmailer->send()) {
                throw new Exception('Error en send mail', -1);
            }
            $phpmailer = null;
            return 0;
        }catch (Exception $e){
            return -1;
        }
    }   

   // ===================
    // Enviar mail Reset
    // ==================
    private function enviarMailRestablecer(){
        try {            
            // url de ofivirtual + ruta de confirmacion
            $url = $this->urlPrincipal.'usuario/resetPassword/restablecer=';
            // cargamos el template html y le pasamos los datos
            $mensaje =file_get_contents("asset/email/resetpassword.html");
            $mensaje = str_replace("%KKK001%", $this->getEmail(), $mensaje);
            $mensaje = str_replace("%KKK002%", '', $mensaje);
            $mensaje = str_replace("KKK003", $url.$this->getToken(), $mensaje);
            $mensaje = str_replace("%KKK004%", $url.$this->getToken(), $mensaje);

            $phpmailer = new PHPMailer(TRUE);            
            $phpmailer->isSMTP();
            // luego cambiar por el smtp de abaco
            $phpmailer->Host = 'smtp.gmail.com';
            $phpmailer->SMTPAuth = true;
            $phpmailer->Port = 465;
            $phpmailer->SMTPSecure = 'ssl';
            $phpmailer->SMTPAuth   = true;
            $phpmailer->Username = 'roberto.arturo.salinas@gmail.com';
            $phpmailer->Password = 'jtbmeerztpiakrxv';
            $phpmailer->setFrom('roberto.arturo.salinas@gmail.com', 'Tucanes en mi Jujuy');
            
            $phpmailer->addAddress($this->getEmail(), $this->getNombre());
            $phpmailer->addReplyTo('rsalinas@telpin.com.ar', 'Tucanes en mi Jujuy');
            //$phpmailer->addEmbeddedImage('asset/email/abaco', 'abaco', 'abaco', 'base64', 'image/jpg');
            $phpmailer->WordWrap = 50;
            $phpmailer->isHTML(true);
            
            $phpmailer->Body = "Restablecimiento de Clave en Tucanes en mi Jujuy";
            $phpmailer->Subject = "Restablecimiento de Clave en Tucanes en mi Jujuy";
            $phpmailer->AltBody = "Restablecimiento de Clave en Tucanes en mi Jujuy";
            $phpmailer->msgHTML($mensaje);
            if (!$phpmailer->send()) {
                echo 'Error en send';
                throw new Exception('Error en send mail', -1);
            }
            $phpmailer = null;
            return 0;
        }catch (Exception $e){
            return -1;
        }
    }       

}
