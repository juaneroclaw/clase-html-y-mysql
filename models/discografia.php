<?php

require_once "DAO/discografiaDAOImpl.php";
require_once "db_manager.php";

Class Discografia
{
    private $id;
    private $nombre;
    private $year;
    private $id_banda;
    private $img;
    private $id_tipo_cd;
    private $nombreBanda;
    private $nombreCd;
    // dbmanager
    private $dbm;
    // lista de barrios en json
    private $listaDiscografia;
    private $listaBanda;
    private $listaTipo;
    // respuesta
    private $error;
    private $mensaje;

    public function __construct($dbm)
    {
        $this->dbm = $dbm;
    }

    public function getDiscografias(){
        $brrDAO = new DiscografiaDAOImpl($this->dbm);
        $brrDAO->getAllDiscografia($this);
        return $this;
    }

    public function getBanda(){
        $brrDAO = new DiscografiaDAOImpl($this->dbm);
        $brrDAO->getBandas($this);
        return $this;
    }

    public function getDiscografiaPorFiltros(){
        $brrDAO = new DiscografiaDAOImpl($this->dbm);
        $brrDAO->getDiscografiaPorFiltros($this);
        return $this;
    }

    public function getTipos(){
        $brrDAO = new DiscografiaDAOImpl($this->dbm);
        $brrDAO->getTipos($this);
        return $this;
    }

       /**
     * Get the value of jsonDiscografia
     */ 
    public function getlistaDiscografia()
    {
        return $this->listaDiscografia;
    }

    /**
     * Set the value of jsonBarrios
     *
     * @return  self
     */ 
    public function setlistaDiscografia($listaDiscografia)
    {
        $this->listaDiscografia = $listaDiscografia;
        return $this;
    }

    public function getlistaBanda()
    {
        return $this->listaBanda;
    }

    /**
     * Set the value of jsonBarrios
     *
     * @return  self
     */ 
    public function setlistaBanda($listaBanda)
    {
        $this->listaBanda = $listaBanda;
        return $this;
    }

    /**
     * Set the value of jsonBarrios
     *
     * @return  self
     */ 
    public function setlistaTipos($listaTipo)
    {
        $this->listaTipo = $listaTipo;
        return $this;
    }
    public function getlistaTipos()
    {
        return $this->listaTipo;
    }

    

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of mensaje
     */ 
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * Set the value of mensaje
     *
     * @return  self
     */ 
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;

        return $this;
    }

    /**
     * Get the value of idCiu
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of idCiu
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of year
     */ 
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set the value of year
     *
     * @return  self
     */ 
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get the value of id_banda
     */ 
    public function getIdBanda()
    {
        return $this->id_banda;
    }

    /**
     * Set the value of id_banda
     *
     * @return  self
     */ 
    public function setIdBanda($id_banda)
    {
        $this->id_banda = $id_banda;

        return $this;
    }

    /**
     * Get the value of img
     */ 
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Set the value of img
     *
     * @return  self
     */ 
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get the value of id_tipo_cd
     */ 
    public function getIdTipoCd()
    {
        return $this->id_tipo_cd;
    }

    /**
     * Set the value of id_tipo_cd
     *
     * @return  self
     */ 
    public function setIdTipoCd($id_tipo_cd)
    {
        $this->id_tipo_cd = $id_tipo_cd;

        return $this;
    }

    /**
     * Get the value of nombreBanda
     */ 
    public function getNombreBanda()
    {
        return $this->nombreBanda;
    }

    /**
     * Set the value of nombreBanda
     *
     * @return  self
     */ 
    public function setNombreBanda($nombreBanda)
    {
        $this->nombreBanda = $nombreBanda;

        return $this;
    }

    /**
     * Get the value of nombreCd
     */ 
    public function getNombreCd()
    {
        return $this->nombreCd;
    }

    /**
     * Set the value of nombreCd
     *
     * @return  self
     */ 
    public function setNombreCd($nombreCd)
    {
        $this->nombreCd = $nombreCd;

        return $this;
    }

    
    /**
     * Get the value of error
     */ 
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set the value of error
     *
     * @return  self
     */ 
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

}