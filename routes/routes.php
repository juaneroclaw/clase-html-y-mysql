<?php


$url_array = explode("/",$_SERVER['REQUEST_URI']);
$url_array = array_filter($url_array);
$method = $_SERVER["REQUEST_METHOD"];


// ===============
// NO HAY PETICION
// ===============

if (count($url_array) == 0){    // no hay ninguna peticion
    $json = array(
        'status' => 404,
        'result' => 'Not Found'
    );

    echo json_encode($json, http_response_code($json['status']));
}


// ===============
// HAY PETICION
// ===============
if (count($url_array) >= 0 && isset($_SERVER["REQUEST_METHOD"])){  
    // GET
    if ($method == 'GET'){
        include "services/get.php";
    }
    // POST
    if ($method == 'POST'){     
        include "services/post.php";
    }
    // PUT
    if ($method == 'PUT'){
        $json = array(
            'status' => 200,
            'result' => 'Solicitud PUT'
        );
    }    
    // DELETE
    if ($method == 'DELETE'){
        $json = array(
            'status' => 200,
            'result' => 'Solicitud DELETE'
        );
    }
    // POST
    if ($method == 'PATCH'){
        $json = array(
            'status' => 200,
            'result' => 'Solicitud PATCH'
        );
    }        
    //echo json_encode($json, http_response_code($json['status']));
}    

return;