<?php

require_once "controllers/get.controller.php";

// configuro DB
$controller = new getController();
$controller->configuraDB();

// =============================
// GET Barrios
// =============================

if (isset($url_array[3]) && ($url_array[3] == "discografia")){
    $response = $controller->getDiscografias();
    echo $response;
    return;
}

// =============================
// GET un Barrio
// =============================

if (isset($url_array[3]) && ($url_array[3] == "discografias")){
    $idBanda = isset($_GET['idBanda']) ? filter_var($_GET['idBanda'], FILTER_SANITIZE_STRING) : '';
    $idCd = isset($_GET['idCd']) ? filter_var($_GET['idCd'], FILTER_SANITIZE_STRING) : '';
    $idBanda = trim($idBanda);
    $idCd = trim($idCd);
    $datos[0]=$idBanda;
    $datos[1]=$idCd;
    $response = $controller->getDiscografia($datos);
    echo $response;
    return;
}

if (isset($url_array[3]) && ($url_array[3] == "bandas")){
    $response = $controller->getBandas();
    echo $response;
    return;
}

if (isset($url_array[3]) && ($url_array[3] == "tipos")){
    $response = $controller->getTipos();
    echo $response;
    return;
}

// =============================
// GET Avistaje
// =============================
if (isset($url_array[1]) && ($url_array[1] == "avistaje")){
    $fechaDesde = isset($_GET['FechaDesde']) ? filter_var($_GET['FechaDesde'], FILTER_SANITIZE_STRING) : '';
    $fechaDesde = trim($fechaDesde);
    $fechaHasta = isset($_GET['FechaHasta']) ? filter_var($_GET['FechaHasta'], FILTER_SANITIZE_STRING) : '';
    $fechaHasta = trim($fechaHasta);
    $Barrios = isset($_GET['Barrios']) ? filter_var($_GET['Barrios'], FILTER_SANITIZE_STRING) : '';
    var_dump($Barrios);
    $arrayBarrios = json_decode($Barrios, true);
    var_dump($arrayBarrios);
    return;
    $response = $controller->getAvistajes();
    echo $response;
}

// =============================
// Info
// =============================
if (isset($url_array[1])){
    if ($url_array[1] == "info.php"){      
        phpinfo();
    }
}


$json = array(
    'status' => 200,
    'result' => 'Solicitud '.$method
);
echo json_encode($url_array[2], http_response_code($json['status']));
return;