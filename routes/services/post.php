<?php

require_once "controllers/post.controller.php";

// configuro DB
$controller = new PostController();
$controller->configuraDB();   

if (isset($url_array[1])) {
    if ($url_array[1] == "usuario") {
        if (isset($url_array[2])) {
            // ==================
            // Registro de Usuario
            // ==================
            if ($url_array[2] == "registro") {
                // obtengo el array post
                $post = getPost();
                $nick = isset($post['nick']) ? filter_var($post['nick'], FILTER_SANITIZE_STRING) : '';
                $nick = trim($nick);
                $email = isset($post['email']) ? filter_var($post['email'], FILTER_SANITIZE_EMAIL) : '';
                $email = trim($email);
                $password = isset($post['password']) ? filter_var($post['password'], FILTER_SANITIZE_STRING) : '';
                $password = trim($password);
                $nombre = isset($post['nombre']) ? filter_var($post['nombre'], FILTER_SANITIZE_STRING) : '';
                $nombre = trim($nombre);
                $response = $controller->registrarUsuario($nombre, $nick, $email, $password);
                echo $response;
                return;
            }
            // ==================
            // Login de Usuario
            // ==================
            if ($url_array[2] == "login") {
                // obtengo el array post
                $post = getPost();
                $email = isset($post['email']) ? filter_var($post['email'], FILTER_SANITIZE_EMAIL) : '';
                $email = trim($email);
                $password = isset($post['password']) ? filter_var($post['password'], FILTER_SANITIZE_STRING) : '';
                $response = $controller->loginUsuario($email, $password);
                echo $response;
                return;
            }
            // ==================
            // Forget pasword
            // ==================
            if ($url_array[2] == "forgetPassword") {
                // obtengo el array post
                $post = getPost();
                $email = isset($post['email']) ? filter_var($post['email'], FILTER_SANITIZE_EMAIL) : '';
                $email = trim($email);
                $response = $controller->forgetPassword($email);
                echo $response;
                return;
            }
            // ==================
            // Reset pasword
            // ==================
            if ($url_array[2] == "resetPassword") {
                // obtengo el array post
                $post = getPost();
                $token = isset($post['token']) ? filter_var($post['token'], FILTER_SANITIZE_STRING) : '';
                $token = trim($token);
                $password = isset($post['password']) ? filter_var($post['password'], FILTER_SANITIZE_STRING) : '';
                $response = $controller->resetPassword($token, $password);
                echo $response;
                return;
            }
            // ==================
            // Actualizar Datos
            // ==================
            if ($url_array[2] == "actualizarDatos") {
                // obtengo el array post
                $post = getPost();
                $token = isset($post['token']) ? filter_var($post['token'], FILTER_SANITIZE_STRING) : '';
                $token = trim($token);
                $newNick = isset($post['nuevoNick']) ? filter_var($post['nuevoNick'], FILTER_SANITIZE_STRING) : '';
                $newNick = trim($newNick);
                $newNombre = isset($post['nuevoNombre']) ? filter_var($post['nuevoNombre'], FILTER_SANITIZE_STRING) : '';
                $newNombre = trim($newNombre);
                $response = $controller->actualizarDatos($token, $newNick, $newNombre);
                echo $response;
                return;
            }
        }
    }
}

// ==================
// Otras peticiones POST
// ==================
$json = array(
    'status' => 200,
    'result' => 'Solicitud '.$method
);
echo json_encode($json, http_response_code($json['status']));
return;


// ==================
// get Body del Post
// ==================
function getPost(){
    if(!empty($_POST))
    {
        // when using application/x-www-form-urlencoded or multipart/form-data as the HTTP Content-Type in the request
        // NOTE: if this is the case and $_POST is empty, check the variables_order in php.ini! - it must contain the letter P
        return $_POST;
    }

    // when using application/json as the HTTP Content-Type in the request 
    $post = json_decode(file_get_contents('php://input'), true);
    if(json_last_error() == JSON_ERROR_NONE)
    {
        return $post;
    }

    return [];
}